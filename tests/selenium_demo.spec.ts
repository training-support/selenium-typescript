/// selenium_demo.spec.ts
/// Open `https://google.com` using Selenium and Mocha

import { Builder, WebDriver } from "selenium-webdriver";

describe("Selenium Demo Test", function () {
    this.timeout(10000);

    let driver: WebDriver;
    
    before(async function() {
        // Initializing Firefox Driver
        driver = await new Builder().forBrowser("firefox").build();
    });

    it("should open https://google.com", async function() {
        await driver.get("https://google.com");
    });

    after(async function() {
        await driver.quit();
    });
});
