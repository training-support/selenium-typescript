/// activity_24.spec.ts
/// # Test Parameterization
/// Using Mocha:
/// Create a new test file and do the following:
/// * Create a test with the following parameters:
// / | Earned | Spent | Expected |
// / | 30     | 10    | 20       |
// / | 20     | 2     | 18       |

import assert from "assert";

describe("Activity 24", function() {
    const tests = [
        {earned: 30, spent: 10, expected: 20},
        {earned: 20, spent: 2, expected: 18},
    ];

    tests.forEach(({earned, spent, expected}) => {
        it(`should have ${expected} after I add ${earned} and spend ${spent}`, function() {
            let wallet = 0;
            wallet += earned;
            wallet -= spent;
            assert.equal(wallet, expected);
        });
    });
});
