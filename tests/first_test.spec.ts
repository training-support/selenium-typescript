import assert from "assert";

describe("Testing Math Functions", function() {
    describe("Square Root", function() {
        it("Should return 5 when you compute the square root of 25", function() {
            assert.equal(Math.sqrt(25), 5);
        });
        it("Should return 7 when you compute the square root of 49", function() {
            assert.equal(Math.sqrt(49), 7);
        });
    });
    describe("Squares", function() {
        it("Should return 100 when the number is 10", function() {
            assert.equal(Math.pow(10, 2), 100);
        });
    });
});
