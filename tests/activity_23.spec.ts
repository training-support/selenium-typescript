/// activity_23.spec.ts
/// # Calculator Test
/// Using Mocha:
/// Create a new test file and write tests to validate the following cases:
/// * Sum of two numbers
/// * Difference of two numbers
/// * Product of two numbers
/// * Quotient of two numbers

import assert from "assert";

describe("Activity 23", function() {
    describe("Testing Addition", function() {
        it("Should be 25 when you add 10 and 15", function() {
            const num1 = 10;
            const num2 = 15;
            assert.equal(num1+num2, 25);
        });
    });
    describe("Testing Subtraction", function() {
        it("Should be 15 when you subtract 35 from 50", function() {
            const num1 = 50;
            const num2 = 35;
            assert.equal(num1-num2, 15);
        });
    });
    describe("Testing Multiplication", function() {
        it("Should be 100 when you multiply 5 and 20", function() {
            const num1 = 5;
            const num2 = 20;
            assert.equal(num1*num2, 100);
        });
    });
    describe("Testing Division", function() {
        it("Should return 20 when you divide 100 by 5", function() {
            const num1 = 100;
            const num2 = 5;
            assert.equal(num1/num2, 20);
        });
    });
});
