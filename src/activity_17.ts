/// activity_17.ts
/// # Dynamic Attributes 1
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/dynamic-attributes
/// * Get the title of the page and print it to the console.
/// * Find the username and password input fields. Type in the credentials:
///    * username: admin
///    * password: password
/// * Wait for login message to appear and print the login message to the console.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/dynamic-attributes");

        // Get the title and print it
        console.log(await driver.getTitle());

        //Find username and password fields
        const userName: WebElement = await driver.findElement(By.xpath("//input[starts-with(@class, 'username')]"));
        const password: WebElement = await driver.findElement(By.xpath("//input[contains(@class, 'password')]"));

        //Type credentials
        await userName.sendKeys("admin");
        await password.sendKeys("password");

        //Click Log in
        await driver.findElement(By.xpath("//button[contains(text(), 'Log in')]")).click();
        
        //Print login message
        const loginMessage: string = await driver.findElement(By.id("action-confirmation")).getText();
        console.log("Login message: " + loginMessage);

    } finally {
        await driver.close();
    }
})();
