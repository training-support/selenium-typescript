/// activity_07.ts
/// # Drag and Drop
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/drag-drop
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Find the ball and simulate a click and drag to move it into "Dropzone 1".
///     * Verify that the ball has entered Dropzone 1.
///     * Once verified, move the ball into "Dropzone 2".
///     * Verify that the ball has entered Dropzone 2.
/// * Close the browser.
/// # NOTE: DO NOT USE THE BUILT-IN DRAG DROP FUNCTION

import { Actions, Builder, By, until, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    const actions: Actions = driver.actions({async: true});

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/drag-drop");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Get references to the ball and dropzones
        const ball: WebElement = await driver.findElement(By.id("draggable"));
        const dropzone1: WebElement = await driver.findElement(By.id("droppable"));
        const dropzone2: WebElement = await driver.findElement(By.id("dropzone2"));

        // Drag the ball into dropzone 1
        await actions.move({origin: ball}).press().move({origin: dropzone1}).release().perform();

        // Verify that dropzone 1 was activated
        await driver.wait(until.elementTextContains(dropzone1.findElement(By.css("p")), "Dropped!"));
        console.log("Dropzone 1 was activated!");

        // Drag the ball into dropzone 2
        await actions.move({origin: ball}).press().move({origin: dropzone2}).release().perform();

        // Verify that dropzone 1 was activated
        await driver.wait(until.elementTextContains(dropzone2.findElement(By.css("p")), "Dropped!"));
        console.log("Dropzone 2 was activated!");

    } finally {
        await driver.close();
    }
})();
