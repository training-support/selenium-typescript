/// activity_02.ts
/// # Sending Input
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/login-form
/// * Get the title of the page and print it to the console.
/// * Find the username field using any locator and enter "admin" into it.
/// * Find the password field using any locator and enter "password" into it.
/// * Find the "Log in" button using any locator and click it.
/// * Close the browser.

import { Builder, By, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();
    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/login-form");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the username field and enter text into it
        await driver.findElement(By.id("username")).sendKeys("admin");

        // Find the password field and enter text into it
        await driver.findElement(By.id("password")).sendKeys("password");

        // Click the login button
        await driver.findElement(By.css("button.ui:nth-child(4)")).click();

    } finally {
        await driver.close();
    }
})();
