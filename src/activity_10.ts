/// activity_10.ts
/// # isDisplayed()
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/dynamic-controls
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Use findElement() to find the checkbox input element.
///     * Use the isDisplayed() method to check if it is visible on the page.
///     * Click the "Remove Checkbox" button.
///     * Print the result of the isDisplayed() method again.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/dynamic-controls");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the checkbox
        const checkbox: WebElement = await driver.findElement(By.xpath("//input[@type=\"checkbox\"]"));

        // Print checkbox state
        console.log("Is the checkbox displayed? " + await checkbox.isDisplayed());

        // Click the toggle checkbox button
        await driver.findElement(By.id("toggleCheckbox")).click();

        // Print checkbox state
        console.log("Is the checkbox displayed? " + await checkbox.isDisplayed());

    } finally {
        await driver.close();
    }
})();
