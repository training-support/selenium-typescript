/// activity_12.ts
/// # isEnabled()
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/dynamic-controls
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Use findElement() to find the text field.
///     * Use the isEnabled() method to check if the text field is enabled.
///     * Click the "Enable Input" button to enable the input field.
///     * Use the isEnabled() method again and print the result.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/dynamic-controls");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the text input
        const input: WebElement = await driver.findElement(By.xpath("//input[@type=\"text\"]"));

        // Print input state
        console.log("Is the input enabled? " + await input.isEnabled());

        // Click the toggle input button
        await driver.findElement(By.id("toggleInput")).click();

        // Print input state
        console.log("Is the input enabled? " + await input.isEnabled());

    } finally {
        await driver.close();
    }
})();
