/// activity_14.ts
/// # Tables 2
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/tables
/// * Get the title of the page and print it to the console.
/// * Using findElements() and xpath on the second table:
///     * Find the number of rows and columns table and print them.
///     * Find and print the cell value at the second row second column.
///     * Click the header of the first column to sort by name.
///     * Find and print the cell value at the second row second column again.
///     * Print the cell values of the table footer.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/tables");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Get columns
        const cols: Array<WebElement> = await driver.findElements(By.xpath("//table[@id='sortableTable']/thead/tr/th"));

        // Get Rows
        const rows: Array<WebElement> = await driver.findElements(By.xpath("//table[@id='sortableTable']/tbody/tr"));
        
        // Print number of rows and columns
        console.log("Table has " + cols.length + " columns and " + rows.length + " rows." );

        //Cell value of second row, second column
        const cellValueBefore: WebElement = await driver.findElement(By.xpath("//table[@id='sortableTable']/tbody/tr[2]/td[2]"));
        console.log("Second row, second column value(Before sorting): " + await cellValueBefore.getText());
        
        //Sort the table
        await driver.findElement(By.xpath("//table[@id='sortableTable']/thead/tr/th[2]")).click();

        //Print the value again
        const cellValueAfter: WebElement = await driver.findElement(By.xpath("//table[@id='sortableTable']/tbody/tr[2]/td[2]"));
        console.log("Second row, second column value(After sorting): " + await cellValueAfter.getText());

        //Print footer cell value
        const footer: WebElement = await driver.findElement(By.xpath("//table[@id='sortableTable']/tfoot/tr"));
        console.log("Table footer values: " + await footer.getText());        

    } finally {
        await driver.close();
    }
})();
