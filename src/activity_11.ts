/// activity_11.ts
/// # isSelected()
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/dynamic-controls
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Use findElement() to find the checkbox input element.
///     * Use the isSelected() method to check if the checkbox is selected.
///     * Click the checkbox to select it.
///     * Use the isSelected() method again and print the result.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/dynamic-controls");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the checkbox
        const checkbox: WebElement = await driver.findElement(By.xpath("//input[@type=\"checkbox\"]"));

        // Print checkbox state
        console.log("Is the checkbox selected? " + await checkbox.isSelected());

        // Select the checkbox
        await checkbox.click();

        // Print checkbox state
        console.log("Is the checkbox selected? " + await checkbox.isSelected());

    } finally {
        await driver.close();
    }
})();
