/// activity_21.ts
/// # Tab Opener
/// Using Selenium:
/// * Open a new browser to https://www.training-support.net/selenium/tab-opener
/// * Get the title of the page and print it to the console.
/// * Find the button to open a new tab and click it.
/// * Wait for the new tab to open.
/// * Print all the handles.
/// * Switch to the newly opened tab, print it's title and heading.
/// * Repeat the steps by clicking the button in the new tab page.
/// * Close the browser using quit().

import { Builder, By, until, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/tab-opener");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the button to open a new tab and click it
        await driver.findElement(By.id("launcher")).click();
        
        // Wait for the new tab to open
        await driver.wait(
            async () => (await driver.getAllWindowHandles()).length === 2,
            10000
        );

        // Get all window handles
        let windows: string[] = await driver.getAllWindowHandles();

        // Switch to the new tab
        await driver.switchTo().window(windows[1]);

        // Wait for the new page to load
        await driver.wait(until.titleIs("Newtab"), 10000);
        await driver.wait(until.elementLocated(By.id("actionButton")), 10000);

        // Print the title and heading of the new page
        console.log("New Title: " + await driver.getTitle());
        console.log("New Heading: " + await driver.findElement(By.css("div.content:nth-child(2)")).getText());

        // Find the button to open a new tab and click it
        await driver.findElement(By.id("actionButton")).click();
        
        // Wait for the new tab to open
        await driver.wait(
            async () => (await driver.getAllWindowHandles()).length === 3,
            10000
        );

        // Get all window handles
        windows = await driver.getAllWindowHandles();

        // Switch to the new tab
        await driver.switchTo().window(windows[2]);

        // Wait for the new page to load
        await driver.wait(until.titleIs("Newtab2"), 10000);
        await driver.wait(until.elementLocated(By.css("i.thumbs.up.icon")), 10000);

        // Print the title and heading of the new page
        console.log("New Title: " + await driver.getTitle());
        console.log("New Heading: " + await driver.findElement(By.css("div.content:nth-child(2)")).getText());

    } finally {
        await driver.quit();
    }
})();
