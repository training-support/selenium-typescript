/// activity_01.ts
/// # Clicking a Link
/// Using Selenium:
/// * Open a new browser to https://training-support.net.
/// * Get the title of the page and print it to the console.
/// * Use findElement with css to find a link on the page.
/// * Click on that link.
/// * Get the title of the new page and print it to the console.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();
    try {
        // Navigate to the page
        await driver.get("https://training-support.net");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the link on the page
        const link: WebElement = await driver.findElement(By.css("#about-link"));

        // Click the link
        await link.click();

        // Get the title and print it to the console
        console.log(await driver.getTitle());
    } finally {
        await driver.close();
    }
})();
