/// activity_18.ts
/// # Dynamic Attributes 2
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/dynamic-attributes
/// * Get the title of the page and print it to the console.
/// * Find the input fields of the Sign Up form.
/// * Fill in the details in the fields with your own data.
/// * Wait for success message to appear and print it to the console.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/dynamic-attributes");

        // Get the title and print it
        console.log(await driver.getTitle());

        //Find username and password fields
        const userName: WebElement = await driver.findElement(By.xpath("//input[contains(@class, '-username')]"));
        const password: WebElement = await driver.findElement(By.xpath("//input[contains(@class, '-password')]"));
        const confirmPassword: WebElement = await driver.findElement(By.xpath("//label[text() = 'Confirm Password']/following::input"));
        const email: WebElement = await driver.findElement(By.xpath("//label[contains(text(), 'mail')]/following-sibling::input"));
        
        //Type credentials
        await userName.sendKeys("NewUser");
        await password.sendKeys("Password");
        await confirmPassword.sendKeys("Password");
        await email.sendKeys("real_email@xyz.com");
        
        //Click Sign Up
        await driver.findElement(By.xpath("//button[contains(text(), 'Sign Up')]")).click();
        
        //Print login message
        const loginMessage: string = await driver.findElement(By.id("action-confirmation")).getText();
        console.log("Login message: " + loginMessage);

    } finally {
        await driver.close();
    }
})();
