/// hello_world.ts
/// Open `https://training-support.net` using Selenium

import { Builder, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    // Build a new WebDriver
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    // Use driver.get() to open the browser to a page
    await driver.get("https://training-support.net");

    // Close the browser
    await driver.close();

})();
