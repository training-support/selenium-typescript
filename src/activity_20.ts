/// activity_20.ts
/// # Alerts 2
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/javascript-alerts
/// * Get the title of the page and print it to the console.
/// * Use findElement() to find the button to open a PROMPT alert and click it.
/// * Switch the focus from the main window to the Alert box and get the text in it and print it.
/// * Use sendKeys() to type in "Awesome!"
/// * Close the alert with alert.accept().
/// * Close the browser.

import { Alert, Builder, By, until, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/javascript-alerts");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Click the prompt button
        await driver.findElement(By.id("prompt")).click();

        // Wait for the alert to show
        await driver.wait(until.alertIsPresent());
        
        // Switch to the Alert
        const alert: Alert = await driver.switchTo().alert();

        // Get the alert's text and print it
        console.log( await alert.getText() );

        // Type "Awesome!" into the alert
        await alert.sendKeys("Awesome!");

        // Close the alert using dismiss
        await alert.accept();

    } finally {
        await driver.close();
    }
})();
