/// activity_13.ts
/// # Tables 1
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/tables
/// * Get the title of the page and print it to the console.
/// * Using findElements() and xpath on the first table:
///     * Find the number of rows and columns table and print them.
///     * Find and print all the cell values in the third row of the table.
///     * Find and print the cell value at the second row second column.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/tables");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Get columns
        const cols: Array<WebElement> = await driver.findElements(By.xpath("//table[contains(@class, \"striped\")]/tbody/tr[1]/td"));

        // Get Rows
        const rows: Array<WebElement> = await driver.findElements(By.xpath("//table[contains(@class, \"striped\")]/tbody/tr"));
        
        // Print number of rows and columns
        console.log("Table has " + cols.length + " columns and " + rows.length + " rows." );

        // Print the third row
        const thirdRow: Array<WebElement> = await driver.findElements(By.xpath("//table[contains(@class, 'striped')]/tbody/tr[3]/td"));
        for( const element of thirdRow ) {
            console.log(await element.getText() + ", ");
        }

        // Print the value at the 2nd row, 2nd column
        const cellValue2_2: WebElement = driver.findElement(By.xpath("//table[contains(@class, 'striped')]/tbody/tr[2]/td[2]"));
        console.log("Second row, second column element is: " + await cellValue2_2.getText());

    } finally {
        await driver.close();
    }
})();
