/// activity_04.ts
/// # Target Practice
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/target-practice
/// * Get the title of the page and print it to the console.
/// * Using xpath:
///     * Find the 3rd header on the page and print it's text to the console.
///     * Find the 5th header on the page and print it's color.
/// * Find the violet button and print all it's classes.
/// * Find the grey button and print it's text.
/// * Close the browser.

import { Builder, By, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();
    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/target-practice");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the 3rd header using xpath
        console.log(await (driver.findElement(By.xpath("//h3"))).getText());

        // Find the 5th header and print it's color
        console.log(await (driver.findElement(By.xpath("//h5"))).getCssValue("color"));

        // Find the violet button and print all it's classes
        console.log(await (driver.findElement(By.css("button.violet"))).getAttribute("class"));

        // Find the grey button and print all it's classes
        console.log(await (driver.findElement(By.css("button.grey"))).getText());

    } finally {
        await driver.close();
    }
})();
