/// activity_03.ts
/// # Using Xpaths
/// Using Selenium:
/// * Repeat the last activity, but strictly use xpaths.

import { Builder, By, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();
    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/login-form");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the username field and enter text into it
        await driver.findElement(By.xpath("//input[@id='username']")).sendKeys("admin");

        // Find the password field and enter text into it
        await driver.findElement(By.xpath("//input[@id='password']")).sendKeys("password");

        // Click the login button
        await driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div/button")).click();

    } finally {
        await driver.close();
    }
})();
