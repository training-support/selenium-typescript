/// activity_22.ts
/// # Popups
/// Using Selenium:
/// * Open a new browser to https://www.training-support.net/selenium/popups
/// * Print the title of the page.
/// * Find the Sign in button and hover over it. Print the tooltip message.
/// * Click the button to open the Sign in form.
/// * Enter the credentials
///     * username: admin
///     * password: password
/// * Click login and print the message on the page after logging in.
/// * Finally, close the browser.

import { Actions, Builder, By, until, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    const actions: Actions = driver.actions({async: true});

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/popups");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find Sign in button
        const button: WebElement = await driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/button"));
        
        // Hover over button
        await actions.move({origin: button}).perform();

        const tooltipText: string = await button.getAttribute("data-tooltip");
        console.log("Tooltip text: " + tooltipText);
        
        // Click on the button
        await button.click();
        
        // Wait for modal to appear
        await driver.wait(until.elementLocated(By.id("signInModal")));
        
        // Find username and pasword and fill in the details
        await driver.findElement(By.id("username")).sendKeys("admin");
        await driver.findElement(By.id("password")).sendKeys("password");
        await driver.findElement(By.xpath("//button[text()='Log in']")).click();
        
        // Read the login message
        const message: string = await driver.findElement(By.id("action-confirmation")).getText();
        console.log(message);

    } finally {
        await driver.close();
    }
})();
