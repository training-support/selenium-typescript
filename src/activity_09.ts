/// activity_09.ts
/// # Waits 2
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/ajax
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Find and click the "Change content" button on the page.
///     * Wait for the text to say "HELLO!". Print the message that appears on the page.
///     * Wait for the text to change to contain "I'm late!". Print the new message on the page.
/// * Close the browser.

import { Builder, By, until, WebDriver } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/ajax");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find and click the button
        await driver.findElement(By.xpath("//button[contains(@class, \"violet\")]")).click();

        // Wait for text to load
        await driver.wait(until.elementTextContains(await driver.findElement(By.id("ajax-content")), "HELLO!"));
        
        // Print the Text
        const ajaxText: string = await driver.findElement(By.id("ajax-content")).getText();
        console.log(ajaxText);

        // Wait for late data
        await driver.wait(until.elementTextContains(await driver.findElement(By.id("ajax-content")), "I'm late!"));

        // Print the text
        const lateText: string = await driver.findElement(By.id("ajax-content")).getText();
        console.log(lateText);

    } finally {
        await driver.close();
    }
})();
