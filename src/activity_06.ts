/// activity_06.ts
/// # Input Event 2
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/input-events
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Press the key of first letter of your name in caps
///     * Press CTRL+a and the CTRL+c to copy all the text on the page.
///     * (Paste the text in a text editor to verify results.)
/// * Close the browser.

import { Actions, Builder, By, Key, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    const actions: Actions = driver.actions({async: true});

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/input-events");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Get the Input Area
        const pressedKey: WebElement = await driver.findElement(By.id("keyPressed"));

        // Create an action sequence for typing your name
        await actions.sendKeys("A").perform();
        
        // Get the entered text
        const pressedKeyText: string = await pressedKey.getText();

        // Print the text
        console.log("Pressed Key Is: " + pressedKeyText);

        // Create an action sequence to copy all the text on the screen
        await actions.keyDown(Key.CONTROL).sendKeys("ac").keyUp(Key.CONTROL).perform();

    } finally {
        await driver.close();
    }
})();
