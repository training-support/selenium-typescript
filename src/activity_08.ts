/// activity_08.ts
/// # Waits 1
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/dynamic-controls
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Find the checkbox toggle button and click it.
///     * Wait till the checkbox disappears.
///     * Click the button again. Wait till it appears and check the checkbox.
/// * Close the browser.

import { Builder, By, until, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/dynamic-controls");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Get a reference to the checkbox
        const checkbox: WebElement = await driver.findElement(By.xpath("//div[@id=\"dynamicCheckbox\"]/input"));

        // Get a reference to the toggle checkbox button
        const toggleCheckboxButton: WebElement = await driver.findElement(By.id("toggleCheckbox"));

        // Click the toggle button
        await toggleCheckboxButton.click();

        // Wait for checkbox to disappear
        await driver.wait(until.elementIsNotVisible(checkbox));

        // Click the toggle button again
        await toggleCheckboxButton.click();

        // Wait for the checkbox to appear and click it
        await driver.wait(until.elementIsVisible(checkbox));
        await checkbox.click();

    } finally {
        await driver.close();
    }
})();
