/// activity_15.ts
/// # Selects 1
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/selects
/// * Get the title of the page and print it to the console.
/// * On the Single Select:
///     * Select the second option using the visible text.
///     * Select the third option using the index.
///     * Select the fourth option using the value.
///     * Get all the options and print them to the console.
/// * Close the browser.

import { Builder, By, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/selects");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Select the second option using visible text
        await driver.findElement(By.xpath("//select[@id=\"single-select\"]/option[text()=\"Option 2\"]")).click();

        // Select the third option using index
        const options: WebElement[] = await driver.findElements(By.xpath("//select[@id=\"single-select\"]/option"));
        await options[3].click();

        // Select the fourth option using value
        await driver.findElement(By.xpath("//select[@id=\"single-select\"]/option[@value=\"4\"]")).click();

        // Print all the options into the console
        console.log("The options are:");
        for(const option of options){
            console.log(await option.getText());
        }

    } finally {
        await driver.close();
    }
})();
