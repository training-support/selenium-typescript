/// activity_05.ts
/// # Input Event 1
/// Using Selenium:
/// * Open a new browser to https://www.training-support.net/selenium/input-events
/// * Get the title of the page and print it to the console.
/// * On the page, perform:
///     * Left click and print the value of the side in the front.
///     * Double click to show a random side and print the number.
///     * Right click and print the value shown on the front of the cube.
/// * Close the browser.

import { Actions, Builder, By, until, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    const actions: Actions = driver.actions({async: true});

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/input-events");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Find the Cube
        const cube: WebElement = await driver.findElement(By.id("wrapD3Cube"));

        // Left Click
        await actions.click(cube).perform();
        await driver.wait(until.elementLocated(By.className("active")));
        let cubeVal: WebElement = await driver.findElement(By.className("active"));
        console.log("On Left Click: " + await cubeVal.getText());

        // Double Click
        await actions.doubleClick(cube).perform();
        cubeVal = await driver.findElement(By.className("active"));
        console.log("On Double Click: " + await cubeVal.getText());

        // Right Click
        await actions.contextClick(cube).perform();
        cubeVal = await driver.findElement(By.className("active"));
        console.log("On Right Click: " + await cubeVal.getText());        

    } finally {
        await driver.close();
    }
})();
