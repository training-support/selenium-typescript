/// activity_16.ts
/// # Selects 2
/// Using Selenium:
/// * Open a new browser to https://training-support.net/selenium/selects
/// * Get the title of the page and print it to the console.
/// * On the Multi Select:
///     * Select the "JavaScript" option using the visible text.
///     * Select the 4th, 5th and 6th options using the index.
///     * Select the "NodeJS" option using the value.
///     * Deselect the 5th option using index.
/// * Close the browser.

import { Actions, Builder, By, Key, WebDriver, WebElement } from "selenium-webdriver";

(async function(): Promise<void> {
    const driver: WebDriver = await new Builder().forBrowser("firefox").build();

    const actions: Actions = driver.actions({async: true});

    try {
        // Navigate to the page
        await driver.get("https://training-support.net/selenium/selects");

        // Get the title and print it
        console.log(await driver.getTitle());

        // Hold the control key down
        await actions.keyDown(Key.CONTROL).perform();

        // Select the JavaScript option using visible text
        await driver.findElement(By.xpath("//select[@id=\"multi-select\"]/option[text()=\"Javascript\"]")).click();

        // Select the 4th, 5th and 6th elements based on index
        const options: WebElement[] = await driver.findElements(By.xpath("//select[@id=\"multi-select\"]/option"));
        await options[3].click();
        await options[4].click();
        await options[5].click();

        // Select NodeJS option using value
        await driver.findElement(By.xpath("//select[@id=\"multi-select\"]/option[@value=\"node\"]")).click();

        // Deselect the 5th option using index
        await options[4].click();

        // Release the control key
        await actions.keyUp(Key.CONTROL).perform();

    } finally {
        await driver.close();
    }
})();
